package com.xing.export.ui;

import com.xing.export.AppManager;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.Toast;

/**
 * 应用程序Activity的基类
 * @author tianbaoxing
 * @version 1.0
 * @created 2012-10-16
 */
public class BaseActivity extends Activity{

	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//创建Activity时候，添加Activity到堆栈
		AppManager.getAppManager().addActivity(this);
	}
	// onCreate完成后被调用，用来回复UI状态
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}
	//当activity对用户即将可见的时候调用。
	@Override
	protected void onStart() {
		super.onStart();
	}
	//当activity将要与用户交互时调用此方法，此时activity在activity栈的栈顶，用户输入已经 可以传递给它
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}
	// Activity即将移出栈顶保留UI状态时调用此方法
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	//当系统要启动一个其他的activity时调用（其他的activity显示之前），这个方法被用来提交那些持久数据的改变
	//、停止动画、和其他占用 CPU资源的东西。由于下一个activity在这个方法返回之前不会resumed，
	//所以实现这个方法时代码执行要尽可能快。
	@Override
	public void onPause() {
		super.onPause();
	}
	//当另外一个activity恢复并遮盖住此activity,导致其对用户不再可见时调用。
	//一个新activity启动、其它activity被切换至前景、当前activity被销毁时都会发生这种场景。
	@Override
	public void onStop() {
		super.onStop();
	}
	//在activity被销毁前所调用的最后一个方法，当进程终止时会出现这种情况
	@Override
	protected void onDestroy() {
		super.onDestroy();
		//结束Activity&从堆栈中移除
		AppManager.getAppManager().finishActivity(this);
	}
	
}
