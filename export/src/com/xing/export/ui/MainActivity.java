package com.xing.export.ui;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.xing.export.R;
import com.xing.export.util.ExportUtil;

public class MainActivity extends BaseActivity {
	
	private Button address_export_txt_btn;
	private Button address_export_excel_btn;
	private Button address_export_email_btn;
	public  static String EXPORT_TYPE ="export_type";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		address_export_txt_btn = (Button) findViewById(R.id.address_export_txt);
		address_export_txt_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), ContactsActivity.class);
				intent.putExtra(EXPORT_TYPE, ContactsActivity.EXPORT_TXT);
				startActivity(intent);
			}
		});
		
		address_export_excel_btn = (Button) findViewById(R.id.address_export_excel);
		address_export_excel_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), ContactsActivity.class);
				intent.putExtra(EXPORT_TYPE, ContactsActivity.EXPORT_EXCEL);
				startActivity(intent);
			}
		});
		
		address_export_email_btn = (Button) findViewById(R.id.address_export_mail);
		address_export_email_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String body = ExportUtil.ListToString(ExportUtil.findPhoneContacts(v.getContext(), getResources()));
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("text/plain"); //模拟器
//				i.setType("message/rfc822") ; //真机
				i.putExtra(Intent.EXTRA_EMAIL, new String[]{"136641953@qq.com"});
				i.putExtra(Intent.EXTRA_SUBJECT,"通讯录信息");
				i.putExtra(Intent.EXTRA_TEXT,body);
				startActivity(Intent.createChooser(i, "通讯录信息"));
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
