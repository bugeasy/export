package com.xing.export.adapter;


	import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xing.export.R;
import com.xing.export.bean.PhoneBean;

	/**
	 * 导出通讯录 适配器
	 */
	public class ContactsAdapter  extends BaseAdapter {
		private Context 					context;//运行上下文
		private List<PhoneBean> 				listItems;//数据集合
		private LayoutInflater 				listContainer;//视图容器
		private int 						itemViewResource;//自定义项视图源 
		static class ListItemView{				//自定义控件集合  
			    ImageView iamge = null;
			    TextView title = null;
			    TextView text = null;
		 }  

		/**
		 * 实例化Adapter
		 * @param context
		 * @param data
		 * @param resource
		 */
		public ContactsAdapter(Context context, List<PhoneBean> data,int resource) {
			this.context = context;			
			this.listContainer = LayoutInflater.from(context);	//创建视图容器并设置上下文
			this.itemViewResource = resource;
			this.listItems = data;
		}
		
		public int getCount() {
			return listItems.size();
		}

		public Object getItem(int arg0) {
			return null;
		}

		public long getItemId(int arg0) {
			return 0;
		}
		
		/**
		 * ListView Item设置
		 */
		public View getView(int position, View convertView, ViewGroup parent) {
			//自定义视图
			ListItemView  listItemView = null;
			
			if (convertView == null) {
				//获取list_item布局文件的视图
				convertView = listContainer.inflate(this.itemViewResource, null);
				
				listItemView = new ListItemView();
				//获取控件对象
				listItemView.iamge = (ImageView) convertView.findViewById(R.id.color_image);
				listItemView.title = (TextView) convertView.findViewById(R.id.color_title);
				listItemView.text = (TextView) convertView.findViewById(R.id.color_text);
				
				//设置控件集到convertView
				convertView.setTag(listItemView);
			}else {
				listItemView = (ListItemView)convertView.getTag();
			}	
			
			//设置文字和图片
			PhoneBean bean = listItems.get(position);
			
			 //绘制联系人名称
			listItemView.title.setText(bean.getContactName());
		    //绘制联系人号码
			listItemView.text.setText(bean.getPhoneNumber());
		    //绘制联系人头像
			listItemView.iamge.setImageBitmap(bean.getContactPhoto());
			return convertView;
		}
	}