package com.xing.export.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.text.TextUtils;

import com.xing.export.AppManager;
import com.xing.export.R;
import com.xing.export.bean.PhoneBean;

public class ExportUtil {

	  /**获取库Phon表字段**/
    private static final String[] PHONES_PROJECTION = new String[] {
	    Phone.DISPLAY_NAME, Phone.NUMBER, Photo.PHOTO_ID,Phone.CONTACT_ID };
   
    /**联系人显示名称**/
    private static final int PHONES_DISPLAY_NAME_INDEX = 0;
    
    /**电话号码**/
    private static final int PHONES_NUMBER_INDEX = 1;
    
    /**头像ID**/
    private static final int PHONES_PHOTO_ID_INDEX = 2;
   
    /**联系人的ID**/
    private static final int PHONES_CONTACT_ID_INDEX = 3;
    
	  /**得到手机通讯录联系人信息**/
    public static List<PhoneBean> findPhoneContacts(Context mContext,Resources res) {
    	List<PhoneBean> data = new ArrayList<PhoneBean>();
		ContentResolver resolver = mContext.getContentResolver();
	
		// 获取手机联系人
		Cursor phoneCursor = resolver.query(Phone.CONTENT_URI,PHONES_PROJECTION, null, null, null);
	
	
		if (phoneCursor != null) {
				PhoneBean phoneBean = null;
			    while (phoneCursor.moveToNext()) {
		
				//得到手机号码
				String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
				//当手机号码为空的或者为空字段 跳过当前循环
				if (TextUtils.isEmpty(phoneNumber))
				    continue;
				
				//得到联系人名称
				String contactName = phoneCursor.getString(PHONES_DISPLAY_NAME_INDEX);
				
				//得到联系人ID
				Long contactid = phoneCursor.getLong(PHONES_CONTACT_ID_INDEX);
		
				//得到联系人头像ID
				Long photoid = phoneCursor.getLong(PHONES_PHOTO_ID_INDEX);
				
				//得到联系人头像Bitamp
				Bitmap contactPhoto = null;
		
				//photoid 大于0 表示联系人有头像 如果没有给此人设置头像则给他一个默认的
				if(photoid > 0 ) {
				    Uri uri =ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI,contactid);
				    InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(resolver, uri);
				    contactPhoto = BitmapFactory.decodeStream(input);
				}else {
				    contactPhoto = BitmapFactory.decodeResource(res, R.drawable.export);
				}
				phoneBean = new PhoneBean();
				phoneBean.setContactid(contactid);
				phoneBean.setContactName(contactName);
				phoneBean.setContactPhoto(contactPhoto);
				phoneBean.setPhoneNumber(phoneNumber);
				data.add(phoneBean);
			    }
			    phoneCursor.close();
			}
		return data;
    }
    
    /**得到手机SIM卡联系人人信息**/
    public static  void getSIMContacts(Context mContext,Resources res) {
    	List<PhoneBean> data = new ArrayList<PhoneBean>();
    	ContentResolver resolver = mContext.getContentResolver();
		// 获取Sims卡联系人
		Uri uri = Uri.parse("content://icc/adn");
		Cursor phoneCursor = resolver.query(uri, PHONES_PROJECTION, null, null,
			null);
	
		if (phoneCursor != null) {
			PhoneBean phoneBean = null;
		    while (phoneCursor.moveToNext()) {
	
				// 得到手机号码
				String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
				// 当手机号码为空的或者为空字段 跳过当前循环
				if (TextUtils.isEmpty(phoneNumber))
				    continue;
				// 得到联系人名称
				String contactName = phoneCursor
					.getString(PHONES_DISPLAY_NAME_INDEX);
		
				//Sim卡中没有联系人头像
				phoneBean = new PhoneBean();
				phoneBean.setContactName(contactName);
				phoneBean.setPhoneNumber(phoneNumber);
				data.add(phoneBean);
		    }
		    phoneCursor.close();
		}
    }
    
    public static String ListToString (List<PhoneBean> data){
    	String body="";
    	if(data!=null && data.size()>0){
    		body += "名称\t\t"+"电话"+"\r\n";
    		for(PhoneBean bean:data){
    			body += bean.getContactName()+"\t"+bean.getPhoneNumber()+"\r\n";
    		}
    	}
    	return body ;
    }
    
    /**
	 * 退出程序
	 */
	public static void Exit(final Context cont){
		AlertDialog.Builder builder = new AlertDialog.Builder(cont);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setTitle(R.string.sure_or_layout);
		builder.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				//退出
				AppManager.getAppManager().AppExit(cont);
			}
		});
		builder.setNegativeButton(R.string.cancel, null);
		builder.show();
	}
	
	public static void showMessage(final Context cont ,String body){
		AlertDialog.Builder builder = new AlertDialog.Builder(cont);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setTitle(R.string.show_meg);
		builder.setMessage(body);
		builder.setPositiveButton(R.string.sure, null);
//		builder.setNegativeButton(R.string.cancel, null);
		builder.show();
	}

}
