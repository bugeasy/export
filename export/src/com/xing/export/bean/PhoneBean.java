package com.xing.export.bean;

import android.graphics.Bitmap;

public class PhoneBean {
	
	 private String contactName;
	 private String phoneNumber;
	 private Long contactid;
	 private Bitmap contactPhoto;
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Long getContactid() {
		return contactid;
	}
	public void setContactid(Long contactid) {
		this.contactid = contactid;
	}
	public Bitmap getContactPhoto() {
		return contactPhoto;
	}
	public void setContactPhoto(Bitmap contactPhoto) {
		this.contactPhoto = contactPhoto;
	}
	@Override
	public String toString() {
		return "PhoneBean [contactName=" + contactName + ", phoneNumber="
				+ phoneNumber + ", contactid=" + contactid + ", contactPhoto="
				+ contactPhoto + "]";
	}
	 
	

}
